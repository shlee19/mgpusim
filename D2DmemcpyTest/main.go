package main

import (
	"flag"
	"fmt"

	"gitlab.com/akita/mgpusim/driver"
	"gitlab.com/akita/mgpusim/platform"
)

func main() {

	var Datalength = flag.Int("datalength", 8192, "datalength in each GPUs")
	datalength := *Datalength
	engine, gpuDriver := platform.MakeR9NanoBuilder().Build()

	fmt.Printf("datalength : %d \n", datalength)
	gpuDriver.Run()
	SendBuffers := make([]driver.GPUPtr, 4)
	RecvBuffers := make([]driver.GPUPtr, 4)
	context := gpuDriver.Init()
	TempArray := make([]float32, datalength)

	for i := 0; i < 4; i++ {
		for j := 0; j < datalength; j++ {
			TempArray[j] = float32(j)
		}
		gpuDriver.SelectGPU(context, i+1)
		SendBuffers[i] = gpuDriver.AllocateMemory(context, uint64(datalength*4))
		RecvBuffers[i] = gpuDriver.AllocateMemory(context, uint64(datalength*4))

		if i == 0 {
			gpuDriver.MemCopyH2D(context, SendBuffers[i], TempArray)
		}
	}
	t1 := engine.CurrentTime()
	gpuDriver.SelectGPU(context, 1)
	gpuDriver.MemCopyD2D(context, RecvBuffers[1], SendBuffers[0], datalength*4)
	t2 := engine.CurrentTime()

	TempBuffer := make([]float32, datalength)
	//gpuDriver.SelectGPU(context, 2)
	gpuDriver.MemCopyD2H(context, TempBuffer, RecvBuffers[1])
	//gpuDriver.MemCopyD2H(context, TempBuffer, SendBuffers[0])
	//for i := 0; i < datalength; i++ {
	//	fmt.Printf("(%2f)", TempBuffer[i])
	//}
	fmt.Printf(" %0.2f GB/s\n", float64(datalength*4)/(float64(t2-t1)*float64(1000000000)))
}
