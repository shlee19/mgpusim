package main

import (
	"flag"
	"fmt"
	"log"
	"sync"

	"gitlab.com/akita/mgpusim/benchmarks/dnn/kernels/add"
	"gitlab.com/akita/mgpusim/driver"
	"gitlab.com/akita/mgpusim/insts"
	"gitlab.com/akita/mgpusim/kernels"
	"gitlab.com/akita/mgpusim/platform"
)

type AddKernelArgs struct {
	A                   driver.GPUPtr
	B                   driver.GPUPtr
	C                   driver.GPUPtr
	Padding             uint32
	HiddenGlobalOffsetX int64
	HiddenGlobalOffsetY int64
	HiddenGlobalOffsetZ int64
}
type DIVIDKernelArgs struct {
	A                   driver.GPUPtr
	B                   driver.GPUPtr
	C                   driver.GPUPtr
	Padding             uint32
	HiddenGlobalOffsetX int64
	HiddenGlobalOffsetY int64
	HiddenGlobalOffsetZ int64
}

func main() {
	var datalength = flag.Int("datalength", 512, "datalength in each GPUS")
	var chunksize = flag.Int("chunksize", 128, "unit of size between GPUs")
	flag.Parse()
	engine, gpuDriver := platform.MakeR9NanoBuilder().Build()
	gpuDriver.Run()
	context := gpuDriver.Init()
	var Hsaco *insts.HsaCo
	hsacoBytes, err := add.Asset("add_kernels.hsaco")
	if err != nil {
		log.Panic(err)
	}
	Hsaco = kernels.LoadProgramFromMemory(hsacoBytes, "VectorADD")

	DataLength := *datalength
	ChunkSize := *chunksize
	GPUs := 4
	LoopSize := GPUs * ChunkSize
	SendBuffers := make([]driver.GPUPtr, 4)
	RecvBuffers := make([]driver.GPUPtr, 4)

	TempArray := make([]float32, DataLength)

	fmt.Printf("Datalength : %d \n", DataLength)
	for i := 1; i <= 4; i++ {
		gpuDriver.SelectGPU(context, i)

		for j := 0; j < DataLength; j++ {
			TempArray[j] = float32(i)
		}
		SendBuffers[i-1] = gpuDriver.AllocateMemory(context, uint64(DataLength*4))
		RecvBuffers[i-1] = gpuDriver.AllocateMemory(context, uint64(DataLength*4))

		gpuDriver.MemCopyH2D(context, SendBuffers[i-1], TempArray)
	}

	var tempBandwidth1 float64
	var tempBandwidth float64
	for i := 0; i < DataLength/LoopSize; i++ {
		start := engine.CurrentTime()
		PushDataToNextGPU(gpuDriver, context, RecvBuffers, SendBuffers, ChunkSize, LoopSize, i)
		end := engine.CurrentTime()
		//ForDebugging(gpuDriver, context, RecvBuffers, GPUs, DataLength)
		fmt.Printf("copy Bandwidth %0.2f GB/s\n", float64(ChunkSize*4)/(float64(end-start)*float64(1e9)))
		tempBandwidth = float64(ChunkSize*4) / (float64(end-start) * float64(1e9))

		for j := 2; j < GPUs; j++ {
			t1 := engine.CurrentTime()
			ReduceAndCopyToNextGPU(gpuDriver, context, RecvBuffers, SendBuffers, ChunkSize, LoopSize, i, j, Hsaco)
			t2 := engine.CurrentTime()
			fmt.Printf("copy&reduce Bandwidth %0.2f GB/s\n", float64(ChunkSize*4)/(float64(t2-t1)*float64(1e9)))
			tempBandwidth1 = float64(ChunkSize*4) / (float64(t2-t1) * float64(1e9))
		}

		ReduceAndDirectCopyToNextGPU(gpuDriver, context, RecvBuffers, SendBuffers, ChunkSize, LoopSize, i, GPUs, Hsaco)
		for j := 2; j <= GPUs; j++ {
			t3 := engine.CurrentTime()
			DirectCopyToNextGPU(gpuDriver, context, RecvBuffers, SendBuffers, ChunkSize, LoopSize, i, j)
			t4 := engine.CurrentTime()
			fmt.Printf("copy&reduce Bandwidth %0.2f GB/s\n", float64(ChunkSize*4)/(float64(t4-t3)*float64(1e9)))
		}
	}
	fmt.Printf("Bus Bandwidth %0.2f GB/s\n", (tempBandwidth+tempBandwidth1)/float64(2))

}

func DirectCopyToNextGPU(gpuDriver *driver.Driver, context *driver.Context,
	RecvBuffers []driver.GPUPtr, SendBuffers []driver.GPUPtr, ChunkSize int, LoopSize int, step int, subStep int) {
	var wg sync.WaitGroup
	SrcAddressGroup := make([]driver.GPUPtr, 0)
	DstAddressGroup := make([]driver.GPUPtr, 0)
	for i := 0; i < 4; i++ {
		SrcIndex := i%4 + 1     //1, 2, 3, 4
		DstIndex := (i+1)%4 + 1 //2, 3, 4, 1
		//Reserve the Destination address where they was located.
		SrcPtr := SendBuffers[SrcIndex-1] + driver.GPUPtr(step*LoopSize*4+((4-subStep+i+2)%4*ChunkSize)*4)
		DstPtr := SendBuffers[DstIndex-1] + driver.GPUPtr(step*LoopSize*4+((4-subStep+i+2)%4*ChunkSize)*4)
		SrcAddressGroup = append(SrcAddressGroup, SrcPtr)
		DstAddressGroup = append(DstAddressGroup, DstPtr)
	}
	for i := 0; i < 4; i++ {
		wg.Add(1)
		go MultiMemcopy(gpuDriver, context, DstAddressGroup[i], SrcAddressGroup[i], ChunkSize, &wg, i)
	}
	wg.Wait()
}

func MultiMemcopy(gpuDriver *driver.Driver, context *driver.Context,
	recvBuffers driver.GPUPtr, sendBuffers driver.GPUPtr, chunksize int, wg *sync.WaitGroup, i int) {
	defer wg.Done()
	gpuDriver.SelectGPU(context, i+1)
	gpuDriver.MemCopyD2D(context, recvBuffers, sendBuffers, chunksize*4)
}

func ForDebugging(gpuDriver *driver.Driver, context *driver.Context, showBuffer []driver.GPUPtr, numGPU int, datalength int) {
	TotalLength := datalength
	TempBuffer := make([][]float32, 4)
	for i := 0; i < 4; i++ {
		TempBuffer[i] = make([]float32, TotalLength)
	}
	for i := 1; i <= numGPU; i++ {
		gpuDriver.SelectGPU(context, i)
		gpuDriver.MemCopyD2H(context, TempBuffer[i-1], showBuffer[i-1])
	}
	for i := 0; i < 4; i++ {
		fmt.Printf("GPU %d", i+1)
		for j := 0; j < datalength; j++ {
			fmt.Printf("(%2.1f)", TempBuffer[i][j])
		}
		fmt.Println()
	}
	fmt.Println()

}

/*
	for i := 0; i < 4; i++ {
		fmt.Printf("\nGPU %d", i+1)
		gpuDriver.MemCopyD2H(context, TempBuffer, RecvBuffers[i])
		for i := 0; i < DataLength; i++ {
			fmt.Printf("(%.1f)", TempBuffer[i])
		}
	}
*/

//TempBuffer := make([]float32, DataLength)
/*
	for i := 0; i < 4; i++ {
		fmt.Printf("\nGPU %d", i+1)
		gpuDriver.SelectGPU(context, i+1)
		gpuDriver.MemCopyD2H(context, TempBuffer, SendBuffers[i])
		//for i := 0; i < DataLength; i++ {
		//	fmt.Printf("(%.1f)", TempBuffer[i])
		//}
	}
*/
