package main

import (
	"sync"

	"gitlab.com/akita/mgpusim/driver"
)

func PushDataToNextGPU(
	gpuDriver *driver.Driver, context *driver.Context,
	RecvBuffers []driver.GPUPtr, SendBuffers []driver.GPUPtr, ChunkSize int, LoopSize int, step int) {
	var wg sync.WaitGroup
	SrcAddressGroup := make([]driver.GPUPtr, 0)
	DstAddressGroup := make([]driver.GPUPtr, 0)
	for j := 0; j < 4; j++ {
		SrcIndex := j%4 + 1
		DstIndex := (j+1)%4 + 1
		SrcPtr := SendBuffers[SrcIndex-1] + driver.GPUPtr(step*LoopSize*4) + driver.GPUPtr(j*ChunkSize*4)
		DstPtr := RecvBuffers[DstIndex-1] + driver.GPUPtr(step*LoopSize*4) + driver.GPUPtr(j*ChunkSize*4)
		SrcAddressGroup = append(SrcAddressGroup, SrcPtr)
		DstAddressGroup = append(DstAddressGroup, DstPtr)
	}
	for j := 0; j < 4; j++ {
		wg.Add(1)
		go PushDataToEachGPU(gpuDriver, context, DstAddressGroup[j], SrcAddressGroup[j], ChunkSize, &wg, j)
	}
	wg.Wait()
}
func PushDataToEachGPU(gpuDriver *driver.Driver, context *driver.Context,
	recvBuffers driver.GPUPtr, sendBuffers driver.GPUPtr, chunksize int, wg *sync.WaitGroup, i int) {
	defer wg.Done()
	gpuDriver.SelectGPU(context, i+1)
	gpuDriver.MemCopyD2D(context, recvBuffers, sendBuffers, chunksize*4)
}
