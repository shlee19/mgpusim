package main

import (
	"sync"

	"gitlab.com/akita/mgpusim/driver"
	"gitlab.com/akita/mgpusim/insts"
)

func ReduceAndCopyToNextGPU(gpuDriver *driver.Driver, context *driver.Context,
	RecvBuffers []driver.GPUPtr, SendBuffers []driver.GPUPtr, ChunkSize int, LoopSize int, step int, subStep int, Hsaco *insts.HsaCo) {
	var wg sync.WaitGroup
	Src1 := make([]driver.GPUPtr, 0)
	Src2 := make([]driver.GPUPtr, 0)
	Dst := make([]driver.GPUPtr, 0)
	for j := 0; j < 4; j++ {
		StartOfChunkAddress := ((4 - (subStep) + j + 1) % 4) * (1) * ChunkSize * 4
		GPUIndex := j
		TempSrc1Address := RecvBuffers[GPUIndex] + driver.GPUPtr(step*LoopSize*4+StartOfChunkAddress)
		TempSrc2Address := SendBuffers[GPUIndex] + driver.GPUPtr(step*LoopSize*4+StartOfChunkAddress)
		TempDstAddress := RecvBuffers[(GPUIndex+1)%4] + driver.GPUPtr(step*LoopSize*4+StartOfChunkAddress)
		Src1 = append(Src1, TempSrc1Address)
		Src2 = append(Src2, TempSrc2Address)
		Dst = append(Dst, TempDstAddress)
	}
	for j := 0; j < 4; j++ {
		wg.Add(1)
		go ReduceAndCopyToNextGPUToEachGPU(gpuDriver, context, Dst[j], Src1[j], Src2[j], ChunkSize, &wg, Hsaco, j)
	}
	wg.Wait()
}

func ReduceAndCopyToNextGPUToEachGPU(gpuDriver *driver.Driver, context *driver.Context,
	dst driver.GPUPtr, src1 driver.GPUPtr, src2 driver.GPUPtr, chunksize int, wg *sync.WaitGroup, Hsaco *insts.HsaCo, i int) {
	defer wg.Done()
	gpuDriver.SelectGPU(context, i+1)
	queue := gpuDriver.CreateCommandQueue(context)
	kernArg := AddKernelArgs{
		src1,
		src2,
		dst,
		0,
		int64(chunksize), 0, 0,
	}

	gpuDriver.EnqueueLaunchKernel(
		queue,
		Hsaco,
		[3]uint32{uint32(chunksize), 1, 1},
		[3]uint16{128, 1, 1}, &kernArg,
	)
	gpuDriver.DrainCommandQueue(queue)
}

func ReduceAndDirectCopyToNextGPU(gpuDriver *driver.Driver, context *driver.Context,
	RecvBuffers []driver.GPUPtr, SendBuffers []driver.GPUPtr, ChunkSize int, LoopSize int, step int, subStep int, Hsaco *insts.HsaCo) {
	var wg sync.WaitGroup
	Src1 := make([]driver.GPUPtr, 0)
	Src2 := make([]driver.GPUPtr, 0)
	Dst := make([]driver.GPUPtr, 0)
	for j := 0; j < 4; j++ {
		StartOfChunkAddress := ((4 - (subStep) + j + 1) % 4) * (1) * ChunkSize * 4
		GPUIndex := j
		TempSrc1Address := RecvBuffers[GPUIndex] + driver.GPUPtr(step*LoopSize*4+StartOfChunkAddress)
		TempSrc2Address := SendBuffers[GPUIndex] + driver.GPUPtr(step*LoopSize*4+StartOfChunkAddress)
		TempDstAddress := SendBuffers[(GPUIndex+1)%4] + driver.GPUPtr(step*LoopSize*4+StartOfChunkAddress)
		Src1 = append(Src1, TempSrc1Address)
		Src2 = append(Src2, TempSrc2Address)
		Dst = append(Dst, TempDstAddress)
	}
	for j := 0; j < 4; j++ {
		wg.Add(1)
		go ReduceAndDirectCopyToNextGPUToEachGPU(gpuDriver, context, Dst[j], Src1[j], Src2[j], ChunkSize, &wg, Hsaco, j)
	}
	wg.Wait()
}

func ReduceAndDirectCopyToNextGPUToEachGPU(gpuDriver *driver.Driver, context *driver.Context,
	dst driver.GPUPtr, src1 driver.GPUPtr, src2 driver.GPUPtr, chunksize int, wg *sync.WaitGroup, Hsaco *insts.HsaCo, i int) {
	defer wg.Done()
	gpuDriver.SelectGPU(context, i+1)
	queue := gpuDriver.CreateCommandQueue(context)
	kernArg := AddKernelArgs{
		src1,
		src2,
		dst,
		0,
		int64(chunksize), 0, 0,
	}

	gpuDriver.EnqueueLaunchKernel(
		queue,
		Hsaco,
		[3]uint32{uint32(chunksize), 1, 1},
		[3]uint16{128, 1, 1}, &kernArg,
	)
	gpuDriver.DrainCommandQueue(queue)
}
