package main

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mgpusim/benchmarks/heteromark/sumGPU"
	"gitlab.com/akita/mgpusim/driver"
	"gitlab.com/akita/mgpusim/insts"
)

type Communicator struct {
	engine     akita.Engine
	gpuDriver  *driver.Driver
	context    *driver.Context
	ChunkSize  int
	DataLength int
	numGPUs    int
	LoopSize   int

	numSlices      int
	SendBuffers    []driver.GPUPtr
	RecvBuffers    []driver.GPUPtr
	BufferInRouter int

	Pointers  []driver.GPUPtr
	AddKernel *sumGPU.Benchmark

	ContentionSize   int
	Contentions      []driver.GPUPtr
	ContentionKernel *sumGPU.Benchmark

	Hsaco       *insts.HsaCo
	D_Hsaco     *insts.HsaCo
	DivisionNum []driver.GPUPtr
}

func NewBuildCommunicator(engine akita.Engine, gpuDriver *driver.Driver) *Communicator {
	NewCommunicator := new(Communicator)
	NewCommunicator.engine = engine
	NewCommunicator.gpuDriver = gpuDriver

	return NewCommunicator
}
