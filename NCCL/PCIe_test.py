import os

os.system('nohup ./NCCL -optype=1 -datalength=4096 -chunksize=1024 > meshDirLatency_VC10000/latency_16k &')
os.system('nohup ./NCCL -optype=1 -datalength=8192 -chunksize=2048 > meshDirLatency_VC10000/latency_32k &')
os.system('nohup ./NCCL -optype=1 -datalength=16384 -chunksize=4096 > meshDirLatency_VC10000/latency_64k &')
os.system('nohup ./NCCL -optype=1 -datalength=32768 -chunksize=8192 > meshDirLatency_VC10000/latency_128k &')
os.system('nohup ./NCCL -optype=1 -datalength=65536 -chunksize=16384 > meshDirLatency_VC10000/latency_256k &')
os.system('nohup ./NCCL -optype=1 -datalength=131072 -chunksize=32768 > meshDirLatency_VC10000/latency_512k &')
os.system('nohup ./NCCL -optype=1 -datalength=262144 -chunksize=65536 > meshDirLatency_VC10000/latency_1M &')
os.system('nohup ./NCCL -optype=1 -datalength=524288 -chunksize=131072 > meshDirLatency_VC10000/latency_2M &')
os.system('nohup ./NCCL -optype=1 -datalength=1048576 -chunksize=262144 > meshDirLatency_VC10000/latency_4M &')
os.system('nohup ./NCCL -optype=1 -datalength=2097152 -chunksize=524288 > meshDirLatency_VC10000/latency_8M &')
os.system('nohup ./NCCL -optype=1 -datelength=4194304 -chunksize=1048576 > meshDirLatency_VC10000/latency_16M &')

os.system('nohup ./NCCL -optype=1 -datalength=8388608 -chunksize=1048576 > meshDirLatency_VC10000/latency_32M &')
os.system('nohup ./NCCL -optype=1 -datalength=16777216 -chunksize=1048576 > meshDirLatency_VC10000/latency_64M &')
os.system('nohup ./NCCL -optype=1 -datalength=33554432 -chunksize=1048576 > meshDirLatency_VC10000/latency_128M &')
os.system('nohup ./NCCL -optype=1 -datalength=67108864 -chunksize=1048576 > meshDirLatency_VC10000/latency_256M &')
os.system('nohup ./NCCL -optype=1 -datalength=134217728 -chunksize=1048576 > meshDirLatency_VC10000/latency_512M &')
