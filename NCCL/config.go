package main

import "flag"

type Config struct {
	TotalGPUs      int
	DataLength     int
	Chunksize      int
	NumSlices      int
	Acc            int
	Optype         int
	BufferInRouter int
}

func (c *Config) Setting() {
	var TotalGPUs = flag.Int("numGPUs", 4, "Number of GPUs")
	var dataLength = flag.Int("datalength", 512, "DataLength in each GPUs")
	var chunksize = flag.Int("chunksize", 128, "Unit of size between GPUs")
	var numSlices = flag.Int("numSlices", 8, "Number of Slices in each chunk")
	var acc = flag.Int("acc", 1, "GPU mode")
	var optype = flag.Int("optype", 1, "optype of operations")
	var bufferInRouter = flag.Int("buffer", 128, "size of buffer in Router")
	flag.Parse()

	c.TotalGPUs = *TotalGPUs
	c.DataLength = *dataLength
	c.Chunksize = *chunksize
	c.NumSlices = *numSlices
	c.Acc = *acc
	c.Optype = *optype
	c.BufferInRouter = *bufferInRouter
}

func (c *Config) setTotalGPUs(totalGPUs int) {
	c.TotalGPUs = totalGPUs
}
func (c *Config) setDataLength(dataLength int) {
	c.DataLength = dataLength
}
func (c *Config) setChunksize(chunksize int) {
	c.Chunksize = chunksize
}
func (c *Config) setNumSlices(numSlices int) {
	c.NumSlices = numSlices
}
func (c *Config) setAcc(acc int) {
	c.Acc = acc
}
func (c *Config) setOptype(optype int) {
	c.Optype = optype
}
