package main

import (
	"fmt"
	"log"

	"gitlab.com/akita/mgpusim/benchmarks/dnn/kernels/add"
	"gitlab.com/akita/mgpusim/benchmarks/dnn/kernels/division"
	"gitlab.com/akita/mgpusim/driver"
	"gitlab.com/akita/mgpusim/insts"
	"gitlab.com/akita/mgpusim/kernels"
	"gitlab.com/akita/mgpusim/platform"
)

type AddKernelArgs struct {
	A                   driver.GPUPtr
	B                   driver.GPUPtr
	C                   driver.GPUPtr
	Padding             uint32
	HiddenGlobalOffsetX int64
	HiddenGlobalOffsetY int64
	HiddenGlobalOffsetZ int64
}
type DIVIDKernelArgs struct {
	A                   driver.GPUPtr
	B                   driver.GPUPtr
	C                   driver.GPUPtr
	Padding             uint32
	HiddenGlobalOffsetX int64
	HiddenGlobalOffsetY int64
	HiddenGlobalOffsetZ int64
}

func main() {
	config := &Config{}
	config.Setting()

	engine, gpuDriver := platform.MakeR9NanoBuilder().Build()

	gpuDriver.Run()
	DataLength := config.DataLength
	DataSize := DataLength * 4
	BufferSize := DataLength * 4
	ChunkSize := config.Chunksize
	LoopSize := config.TotalGPUs * ChunkSize

	SendBuffers := make([]driver.GPUPtr, config.TotalGPUs*2)
	RecvBuffers := make([]driver.GPUPtr, config.TotalGPUs*2)
	Pointers := make([]driver.GPUPtr, config.TotalGPUs*2)

	context := gpuDriver.Init()
	TempArray := make([]float32, DataLength)

	if config.Acc == 1 {
		if config.TotalGPUs == 4 {
			for i := 1; i <= config.TotalGPUs; i++ {
				for j := 0; j < DataLength; j++ {
					TempArray[j] = float32(i)
				}
				gpuDriver.SelectGPU(context, i)
				SendBuffers[i-1] = gpuDriver.AllocateMemory(context, uint64(DataSize))
				RecvBuffers[i-1] = gpuDriver.AllocateMemory(context, uint64(BufferSize))

				gpuDriver.MemCopyH2D(context, SendBuffers[i-1], TempArray)
				//gpuDriver.MemCopyH2D(context, Pointers[i-1], TempArray)
			}
		}
	}
	divisionBuffers := make([]driver.GPUPtr, 4)
	tempGPUs := make([]float32, 1)
	tempGPUs[0] = float32(4)
	for i := 1; i <= 4; i++ {
		gpuDriver.SelectGPU(context, i)
		divisionBuffers[i-1] = gpuDriver.AllocateMemory(context, 1)
		gpuDriver.MemCopyH2D(context, divisionBuffers[i-1], tempGPUs)
	}
	var Hsaco *insts.HsaCo
	hsacoBytes, err := add.Asset("add_kernels.hsaco")
	if err != nil {
		log.Panic(err)
	}
	Hsaco = kernels.LoadProgramFromMemory(hsacoBytes, "VectorADD")

	var D_Hsaco *insts.HsaCo
	dividehsacoBytes, err := division.Asset("division_kernels.hsaco")
	if err != nil {
		log.Panic(err)
	}
	D_Hsaco = kernels.LoadProgramFromMemory(dividehsacoBytes, "VectorDivision")

	//AddKernel.Length = ChunkSize

	MainCommunicator := NewBuildCommunicator(engine, gpuDriver)
	MainCommunicator.context = context
	MainCommunicator.ChunkSize = ChunkSize
	MainCommunicator.DataLength = DataLength
	MainCommunicator.numGPUs = config.TotalGPUs //4
	MainCommunicator.LoopSize = LoopSize
	MainCommunicator.SendBuffers = SendBuffers
	MainCommunicator.RecvBuffers = RecvBuffers
	MainCommunicator.Pointers = Pointers
	MainCommunicator.Hsaco = Hsaco
	MainCommunicator.D_Hsaco = D_Hsaco
	MainCommunicator.DivisionNum = divisionBuffers
	MainCommunicator.numSlices = config.NumSlices
	MainCommunicator.BufferInRouter = config.BufferInRouter

	switch config.Optype {
	case 1:
		fmt.Println("NCCL_ALLREDUCE")
		NCCL2_KernelAllReduceOperation(*MainCommunicator)
	case 2:
		KernelAllReduceOperation(*MainCommunicator)
	}
}

func ForDebugging(gpuDriver *driver.Driver, context *driver.Context, Pointers []driver.GPUPtr,
	RecvBuffers []driver.GPUPtr, numGPU int, datalength int) {

	TotalLength := datalength
	TempBuffer := make([][]float32, 4)
	for i := 0; i < 4; i++ {
		TempBuffer[i] = make([]float32, TotalLength)
	}

	for i := 1; i <= numGPU; i++ {
		gpuDriver.SelectGPU(context, i)
		gpuDriver.MemCopyD2H(context, TempBuffer[i-1], Pointers[i-1])
	}
	for i := 0; i < 4; i++ {
		fmt.Printf("\nGPU %d", i+1)
		for j := 0; j < datalength; j++ {
			fmt.Printf("(%2.2f)", TempBuffer[i][j])
		}
		fmt.Println()
	}
	fmt.Println()

}
