package main

import (
	"fmt"

	"gitlab.com/akita/mgpusim/driver"
)

func NCCL2_KernelAllReduceOperation(c Communicator) {
	DataLength := c.DataLength
	LoopSize := c.LoopSize
	engine := c.engine
	gpuDriver := c.gpuDriver
	context := c.context
	ChunkSize := c.ChunkSize
	numGPUs := c.numGPUs
	SendBuffers := c.SendBuffers
	RecvBuffers := c.RecvBuffers
	Pointers := c.Pointers
	numSlices := c.numSlices
	fmt.Printf("datalength : %d \n", DataLength)
	t1 := engine.CurrentTime()

	fmt.Printf("iteration : %d \n", DataLength/LoopSize)
	for i := 0; i < DataLength/LoopSize; i++ {
		fmt.Printf("now : %f \n", (engine.CurrentTime())*1e9)
		Time1 := PushDataToNextGPU(engine, gpuDriver, context, ChunkSize, numGPUs, LoopSize, i, SendBuffers, RecvBuffers, Pointers, c.Hsaco)
		//Time1 := PushDataToNextGPUDMA(engine, gpuDriver, context, ChunkSize, numGPUs, LoopSize, i, SendBuffers, RecvBuffers, Pointers, c.Hsaco)
		fmt.Printf("				1st Push Next GPU Time( 0) %0.2f ns \n\n", (Time1)*1e9)
		fmt.Printf("				Bandwidth : %0.2f GB/s \n", float64(ChunkSize*4)/float64(Time1*1e9))

		fmt.Printf("now : %f \n", (engine.CurrentTime())*1e9)
		Time2 := NCCL2_KernelOverlapReduceAndCopyToNextGPU(engine, gpuDriver, context, ChunkSize, numSlices, numGPUs, LoopSize, i, 2, SendBuffers, RecvBuffers, Pointers, c.Hsaco)
		fmt.Printf("				2nd Chunk Time : %0.2f ns \n", Time2*1e9)
		fmt.Printf("				Bandwidth : %0.2f GB/s \n", float64(ChunkSize*4)/float64(Time2*1e9))
		fmt.Scanln()

		fmt.Printf("now : %f \n", (engine.CurrentTime())*1e9)
		Time3 := NCCL2_KernelOverlapReduceAndCopyToNextGPU(engine, gpuDriver, context, ChunkSize, numSlices, numGPUs, LoopSize, i, 3, SendBuffers, RecvBuffers, Pointers, c.Hsaco)
		fmt.Printf("				3th Chunk Time : %0.2f ns \n", Time3*1e9)

		fmt.Printf("now : %f \n", (engine.CurrentTime())*1e9)
		Time4 := NCCL2_KernelOverlapReduceAndDirectCopyToNextGPU(engine, gpuDriver, context, ChunkSize, numSlices, numGPUs, LoopSize, i, numGPUs, SendBuffers, RecvBuffers, Pointers, c.Hsaco)
		fmt.Printf("				4th Chunk Time : %0.2f ns \n", Time4*1e9)
		//tempAddress = i
		allgatherTime := float64(0)
		for j := 2; j <= numGPUs; j++ {
			Time := DirectCopyToNextGPU(engine, gpuDriver, context, ChunkSize, numGPUs, LoopSize, i, j, SendBuffers, RecvBuffers, Pointers, c.Hsaco)
			allgatherTime += Time
		}

		ForDebugging(gpuDriver, context, SendBuffers, SendBuffers, numGPUs, DataLength)
		//fmt.Scanln()
		fmt.Printf("				Third AllGather Time : %0.2f ns \n", allgatherTime*1e9)

	}
	t2 := engine.CurrentTime()
	//ForDebugging(gpuDriver, context, SendBuffers, Pointers, numGPUs, DataLength)
	//fmt.Scanln()
	Bandwidth := float64(DataLength) * 4 / (float64(t2-t1) * 1e9)
	fmt.Printf("Algorithm Bandwidth : %0.2f GB/s \n", Bandwidth)
	fmt.Printf("Bus Bandwidth : %0.2f GB/s \n", Bandwidth*float64(1.5))
	fmt.Printf("latency : %0.2f \n\n", (t2-t1)*1e9)

	//DivideLocalGradients(c)
	//ForDebugging(gpuDriver, context, Pointers, Pointers, numGPUs, DataLength)
	//fmt.Scanln()
}

/*
	if DataLength/LoopSize != 0 {
		i := tempAddress + 1
		ChunkSize = (DataLength % LoopSize) / 4
		PushDataToNextGPU(engine, gpuDriver, context, ChunkSize, numGPUs, LoopSize, i, SendBuffers, RecvBuffers, Pointers, AddKernel)
		NCCL2_KernelOverlapReduceAndCopyToNextGPU(engine, gpuDriver, context, ChunkSize, numSlices, numGPUs, LoopSize, i, 2, SendBuffers, RecvBuffers, Pointers, AddKernel)
		NCCL2_KernelOverlapReduceAndCopyToNextGPU(engine, gpuDriver, context, ChunkSize, numSlices, numGPUs, LoopSize, i, 3, SendBuffers, RecvBuffers, Pointers, AddKernel)
		NCCL2_KernelOverlapReduceAndDirectCopyToNextGPU(engine, gpuDriver, context, ChunkSize, numSlices, numGPUs, LoopSize, i, numGPUs, SendBuffers, RecvBuffers, Pointers, AddKernel)
		for j := 2; j <= numGPUs; j++ {
			DirectCopyToNextGPU(engine, gpuDriver, context, ChunkSize, numGPUs, LoopSize, i, j, SendBuffers, RecvBuffers, Pointers, AddKernel)

		}
	}
*/

func KernelAllReduceOperation(c Communicator) {
	DataLength := c.DataLength
	LoopSize := c.LoopSize
	engine := c.engine
	gpuDriver := c.gpuDriver
	context := c.context
	ChunkSize := c.ChunkSize
	numGPUs := c.numGPUs
	SendBuffers := c.SendBuffers
	RecvBuffers := c.RecvBuffers
	Pointers := c.Pointers
	numSlices := c.numSlices

	KernelTime := float64(0)
	TransferTime := float64(0)
	start := engine.CurrentTime()
	numKernelLaunch := 0

	OneLoopTime := float64(0)
	TimeBuffer := float64(0)
	TimeBuffer2 := float64(0)
	for i := 0; i < DataLength/LoopSize; i++ {
		t1 := float64(0)
		t2 := float64(0)
		t1 = float64(engine.CurrentTime())
		TimeBuffer = PushDataToNextGPU(engine, gpuDriver, context, ChunkSize, numGPUs, LoopSize, i, SendBuffers, RecvBuffers, Pointers, c.Hsaco)
		t2 = float64(engine.CurrentTime())
		firstPushTime := float64(t2 - t1)
		fmt.Printf("				First Push Time %0.2f us \n\n", firstPushTime*1000000)
		fmt.Printf("				Bandwidth : %0.2f GB/s \n", float64(ChunkSize*4)/float64(TimeBuffer*1e9))
		OneLoopTime = OneLoopTime + TimeBuffer

		for j := 2; j < numGPUs; j++ {
			//TimeBuffer, TimeBuffer2 = ReduceAndCopyToNextGPU(engine, gpuDriver, context, ChunkSize, numGPUs, LoopSize, i, j, SendBuffers, RecvBuffers, Pointers, AddKernel)
			t1 = float64(engine.CurrentTime())
			TimeBuffer, TimeBuffer2 = KernelOverlapReduceAndCopyToNextGPU(engine, gpuDriver, context, ChunkSize, numSlices, numGPUs, LoopSize, i, j, SendBuffers, RecvBuffers, Pointers, c.Hsaco)
			OneLoopTime = OneLoopTime + TimeBuffer + TimeBuffer2
			numKernelLaunch++
			t2 = float64(engine.CurrentTime())

			fmt.Printf("				Second Push Time(%2d) %0.2f us\n\n", j-1, float64(t2-t1)*1000000)
		}

		//TimeBuffer, TimeBuffer2 = ReduceAndDirectCopyToNextGPU(engine, gpuDriver, context, ChunkSize, numGPUs, LoopSize, i, numGPUs, SendBuffers, RecvBuffers, Pointers, AddKernel)
		t1 = float64(engine.CurrentTime())
		TimeBuffer, TimeBuffer2 = KernelOverlapReduceAndDirectCopyToNextGPU(engine, gpuDriver, context, ChunkSize, numSlices, numGPUs, LoopSize, i, numGPUs, SendBuffers, RecvBuffers, Pointers, c.Hsaco)
		OneLoopTime = OneLoopTime + TimeBuffer + TimeBuffer2
		t2 = float64(engine.CurrentTime())
		fmt.Printf("				Third Reduce Time %0.12f us \n\n", float64(t2-t1)*1000000)

		numKernelLaunch++
		//ForDebugging(gpuDriver, context, Pointers, RecvBuffers)
		t1 = float64(engine.CurrentTime())
		for j := 2; j <= numGPUs; j++ {
			TimeBuffer = DirectCopyToNextGPU(engine, gpuDriver, context, ChunkSize, numGPUs, LoopSize, i, j, SendBuffers, RecvBuffers, Pointers, c.Hsaco)
			OneLoopTime = OneLoopTime + TimeBuffer
		}
		t2 = float64(engine.CurrentTime())
		allGatherTime := float64(t2 - t1)
		fmt.Printf("				Third AllGather Time : %0.2f us \n", allGatherTime*1000000)
		fmt.Printf("============ONE LOOP TIME : %0.2f us \n", OneLoopTime*1000000)
		OneLoopTime = 0
	}

	end := engine.CurrentTime()

	sizeOfAllReduce := DataLength * 4 //byte
	sizeOfAllgather := DataLength * 4 * 3 / 4
	sumOfData := sizeOfAllReduce + sizeOfAllgather

	normalBandwidth := float64(float64(sumOfData) / float64(end-start))
	gBandwidth := float64(normalBandwidth / 1000000000)
	fmt.Printf("Bandwidth : %f GB/s \n", gBandwidth)

	fmt.Printf("==========Final==========\n")
	fmt.Printf("Total Time %0.2f us\n", float64(end-start)*1000000)
	fmt.Printf("Kernel: %0.8f \n", float64(KernelTime))
	fmt.Printf("Transfer: %0.8f \n", float64(TransferTime))
	fmt.Printf("KernelLaunch Counts: %d \n\n\n\n", numKernelLaunch)
	ForDebugging(gpuDriver, context, Pointers, RecvBuffers, numGPUs, DataLength)
}

func DivideLocalGradients(c Communicator) {
	divideSrc1Group := make([]driver.GPUPtr, 0)
	for i := 0; i < 4; i++ {
		divideSrc1Group = append(divideSrc1Group, c.Pointers[i])
	}

	QueueGroup := make([]*driver.CommandQueue, 0)
	queues := make([]*driver.CommandQueue, 4)

	for i := 0; i < 4; i++ {
		c.gpuDriver.SelectGPU(c.context, i+1)
		queues[i] = c.gpuDriver.CreateCommandQueue(c.context)
		KernArg := DIVIDKernelArgs{
			divideSrc1Group[i],
			c.DivisionNum[i],
			divideSrc1Group[i],
			0,
			int64(c.DataLength), 0, 0,
		}
		c.gpuDriver.EnqueueLaunchKernel(
			queues[i],
			c.D_Hsaco,
			[3]uint32{uint32(c.DataLength), 1, 1},
			[3]uint16{128, 1, 1}, &KernArg,
		)
		QueueGroup = append(QueueGroup, queues[i])
	}
	c.gpuDriver.DrainAllCommandQueues2(QueueGroup, 4)
}
