package main

import (
	"fmt"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mgpusim/driver"
	"gitlab.com/akita/mgpusim/insts"
)

func PushDataToNextGPU(engine akita.Engine, gpuDriver *driver.Driver, context *driver.Context, ChunkSize int,
	numGPUs int, LoopSize int, currentStep int, SendBuffers []driver.GPUPtr, RecvBuffers []driver.GPUPtr,
	Pointers []driver.GPUPtr, HsaCo *insts.HsaCo) float64 {

	SrcAddressGroup := make([]driver.GPUPtr, 0)
	DstAddressGroup := make([]driver.GPUPtr, 0)

	for i := 0; i < numGPUs; i++ {
		SrcIndex := i%numGPUs + 1
		DstIndex := (i+1)%numGPUs + 1
		SrcPtr := SendBuffers[SrcIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+(i*ChunkSize*4))
		DstPtr := RecvBuffers[DstIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+(i*ChunkSize*4))

		SrcAddressGroup = append(SrcAddressGroup, SrcPtr)
		DstAddressGroup = append(DstAddressGroup, DstPtr)
	}
	TransferStart := engine.CurrentTime()
	gpuDriver.MultiMemCopyD2D(context, DstAddressGroup, SrcAddressGroup,
		ChunkSize*4, numGPUs)

	TransferEnd := engine.CurrentTime()
	TransferTime := float64(TransferEnd - TransferStart)

	return float64(TransferTime)
}

func PushDataToNextGPUDMA(engine akita.Engine, gpuDriver *driver.Driver, context *driver.Context, ChunkSize int,
	numGPUs int, LoopSize int, currentStep int, SendBuffers []driver.GPUPtr, RecvBuffers []driver.GPUPtr,
	Pointers []driver.GPUPtr, HsaCo *insts.HsaCo) float64 {

	SrcAddressGroup := make([]driver.GPUPtr, 0)
	DstAddressGroup := make([]driver.GPUPtr, 0)

	for i := 0; i < numGPUs; i++ {
		SrcIndex := i%numGPUs + 1
		DstIndex := (i+1)%numGPUs + 1
		SrcPtr := SendBuffers[SrcIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+(i*ChunkSize*4))
		DstPtr := RecvBuffers[DstIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+(i*ChunkSize*4))

		SrcAddressGroup = append(SrcAddressGroup, SrcPtr)
		DstAddressGroup = append(DstAddressGroup, DstPtr)
	}
	TransferStart := engine.CurrentTime()
	gpuDriver.MultiMemCopyD2DDMA(context, SrcAddressGroup, DstAddressGroup,
		ChunkSize*4, numGPUs)

	TransferEnd := engine.CurrentTime()
	TransferTime := float64(TransferEnd - TransferStart)

	return float64(TransferTime)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

func NCCL2_KernelOverlapReduceAndCopyToNextGPU(engine akita.Engine, gpuDriver *driver.Driver,
	context *driver.Context, ChunkSize int, SlicesNum int,
	numGPUs int, LoopSize int, currentStep int, SubStep int,
	SendBuffers []driver.GPUPtr, RecvBuffers []driver.GPUPtr, Pointers []driver.GPUPtr,
	HsaCo *insts.HsaCo) float64 {
	ReduceSrc1Group := make([]driver.GPUPtr, 0)
	ReduceSrc2Group := make([]driver.GPUPtr, 0)
	ReduceDstGroup := make([]driver.GPUPtr, 0)
	for i := 0; i < numGPUs; i++ {
		//TempSrc1 + TempSrc2 -> TempDst
		//(Recv)   + (Pointer)-> (SendBuffers)
		StartOfChunkAddress := ((numGPUs - (SubStep) + i + 1) % numGPUs) * (1) * ChunkSize * 4
		GPUIndex := i
		TempSrc1Address := RecvBuffers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		TempSrc2Address := SendBuffers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		TempDstAddress := RecvBuffers[(GPUIndex+1)%4] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		ReduceSrc1Group = append(ReduceSrc1Group, TempSrc1Address)
		ReduceSrc2Group = append(ReduceSrc2Group, TempSrc2Address)
		ReduceDstGroup = append(ReduceDstGroup, TempDstAddress)
	}
	QueueGroup := make([]*driver.CommandQueue, 0)
	queues := make([]*driver.CommandQueue, numGPUs)

	for i := 0; i < numGPUs; i++ {
		gpuDriver.SelectGPU(context, i+1)
		queues[i] = gpuDriver.CreateCommandQueue(context)
		kernArg := AddKernelArgs{
			ReduceSrc1Group[i],
			ReduceSrc2Group[i],
			ReduceDstGroup[i],
			//ReduceDstGroup[(i+1)%numGPUs],
			0,
			int64(i * ChunkSize), 0, 0,
		}

		gpuDriver.EnqueueLaunchKernel(
			queues[i],
			HsaCo,
			[3]uint32{uint32(ChunkSize), 1, 1},
			[3]uint16{128, 1, 1}, &kernArg,
		)
		QueueGroup = append(QueueGroup, queues[i])
	}

	t1 := engine.CurrentTime()
	gpuDriver.DrainAllCommandQueues2(QueueGroup, numGPUs)
	t2 := engine.CurrentTime()
	return float64(t2 - t1)
}

func NCCL2_KernelOverlapReduceAndDirectCopyToNextGPU(
	engine akita.Engine, gpuDriver *driver.Driver, context *driver.Context, ChunkSize int, SlicesNum int,
	numGPUs int, LoopSize int, currentStep int, SubStep int,
	SendBuffers []driver.GPUPtr, RecvBuffers []driver.GPUPtr,
	Pointers []driver.GPUPtr, HsaCo *insts.HsaCo) float64 {
	ReduceSrc1Group := make([]driver.GPUPtr, 0)
	ReduceSrc2Group := make([]driver.GPUPtr, 0)
	ReduceDstGroup := make([]driver.GPUPtr, 0)

	for i := 0; i < numGPUs; i++ {
		//TempSrc1 + TempSrc2 -> TempDst
		//(Recv)   + (Pointer)-> (SendBuffers)
		StartOfChunkAddress := ((numGPUs - (SubStep) + i + 1) % numGPUs) * (1) * ChunkSize * 4
		GPUIndex := i
		TempSrc1Address := RecvBuffers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		TempSrc2Address := SendBuffers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		TempDstAddress := SendBuffers[(GPUIndex+1)%4] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		ReduceSrc1Group = append(ReduceSrc1Group, TempSrc1Address)
		ReduceSrc2Group = append(ReduceSrc2Group, TempSrc2Address)
		ReduceDstGroup = append(ReduceDstGroup, TempDstAddress)
	}
	QueueGroup := make([]*driver.CommandQueue, 0)
	queues := make([]*driver.CommandQueue, numGPUs)
	for i := 0; i < numGPUs; i++ {
		gpuDriver.SelectGPU(context, i+1)
		queues[i] = gpuDriver.CreateCommandQueue(context)
		kernArg := AddKernelArgs{
			ReduceSrc1Group[i],
			ReduceSrc2Group[i],
			ReduceDstGroup[i],
			//ReduceDstGroup[(i+1)%numGPUs],
			0,
			int64(i * ChunkSize), 0, 0,
		}

		gpuDriver.EnqueueLaunchKernel(
			queues[i],
			HsaCo,
			[3]uint32{uint32(ChunkSize), 1, 1},
			[3]uint16{128, 1, 1}, &kernArg,
		)
		QueueGroup = append(QueueGroup, queues[i])
	}
	t1 := engine.CurrentTime()
	gpuDriver.DrainAllCommandQueues2(QueueGroup, numGPUs)
	t2 := engine.CurrentTime()

	return float64(t2 - t1)
}

func DirectCopyToNextGPU(engine akita.Engine, gpuDriver *driver.Driver, context *driver.Context, ChunkSize int,
	numGPUs int, LoopSize int, currentStep int, SubStep int, SendBuffers []driver.GPUPtr,
	RecvBuffers []driver.GPUPtr, Pointers []driver.GPUPtr, HsaCo *insts.HsaCo) float64 {

	SrcAddressGroup := make([]driver.GPUPtr, 0)
	DstAddressGroup := make([]driver.GPUPtr, 0)

	for i := 0; i < numGPUs; i++ {
		SrcIndex := i%numGPUs + 1     //1, 2, 3, 4
		DstIndex := (i+1)%numGPUs + 1 //2, 3, 4, 1
		//Reserve the Destination address where they was located.
		SrcPtr := SendBuffers[SrcIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+((numGPUs-SubStep+i+2)%numGPUs*ChunkSize)*4)
		DstPtr := SendBuffers[DstIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+((numGPUs-SubStep+i+2)%numGPUs*ChunkSize)*4)
		SrcAddressGroup = append(SrcAddressGroup, SrcPtr)
		DstAddressGroup = append(DstAddressGroup, DstPtr)
	}

	TransferStart := engine.CurrentTime()
	//gpuDriver.MultiMemCopyD2D(engine, context, SrcAddressGroup, DstAddressGroup, ChunkSize*4, numGPUs)
	gpuDriver.MultiMemCopyD2D(context, DstAddressGroup, SrcAddressGroup, ChunkSize*4, numGPUs)

	//ChunkSize*4 --> When they communicated by this unit.
	TransferEnd := engine.CurrentTime()

	TransferTime := float64(TransferEnd - TransferStart)
	return float64(TransferTime)
}

func KernelOverlapReduceAndCopyToNextGPU(engine akita.Engine, gpuDriver *driver.Driver,
	context *driver.Context, ChunkSize int, SlicesNum int,
	numGPUs int, LoopSize int, currentStep int, SubStep int,
	SendBuffers []driver.GPUPtr, RecvBuffers []driver.GPUPtr, Pointers []driver.GPUPtr,
	HsaCo *insts.HsaCo) (float64, float64) {

	ReduceSrc1Group := make([]driver.GPUPtr, 0)
	ReduceSrc2Group := make([]driver.GPUPtr, 0)
	ReduceDstGroup := make([]driver.GPUPtr, 0)

	SrcAddressGroup := make([]driver.GPUPtr, 0)
	DstAddressGroup := make([]driver.GPUPtr, 0)

	numSlices := SlicesNum
	SliceSize := ChunkSize / numSlices

	t1 := float64(0)
	t2 := float64(0)

	//////////////////////////////////////////////
	//////////Reduction time per slices///////////
	//////////////////////////////////////////////

	for i := 0; i < numGPUs; i++ {
		GPUIndex := i
		StartOfChunkAddress := ((numGPUs - (SubStep) + i + 1) % numGPUs) * (1) * ChunkSize * 4
		TempSrc1Address := RecvBuffers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		TempSrc2Address := Pointers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		//TempDstAddress := SendBuffers[(GPUIndex+1)%4] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		TempDstAddress := SendBuffers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)

		ReduceSrc1Group = append(ReduceSrc1Group, TempSrc1Address)
		ReduceSrc2Group = append(ReduceSrc2Group, TempSrc2Address)
		ReduceDstGroup = append(ReduceDstGroup, TempDstAddress)

		//At first, previous level copy the data from SendBuffers[i] to RecvBuffers[i+1], and Pointers buffers already have a src[i]'s
		//RecvBuffers[i] + PointerBuffers[i] => SendBuffers[i]
	}

	QueueGroup := make([]*driver.CommandQueue, 0)
	queues := make([]*driver.CommandQueue, numGPUs)
	for i := 0; i < numGPUs; i++ {
		gpuDriver.SelectGPU(context, i+1)
		queues[i] = gpuDriver.CreateCommandQueue(context)
		kernArg := AddKernelArgs{
			ReduceSrc1Group[i],
			ReduceSrc2Group[i],
			ReduceDstGroup[i],
			//ReduceDstGroup[(i+1)%numGPUs],
			0,
			int64(i * SliceSize), 0, 0,
		}

		gpuDriver.EnqueueLaunchKernel(
			queues[i],
			HsaCo,
			[3]uint32{uint32(SliceSize), 1, 1},
			[3]uint16{128, 1, 1}, &kernArg,
		)
		QueueGroup = append(QueueGroup, queues[i])
	}

	t1 = float64(engine.CurrentTime())
	//Error Point(shlee)
	gpuDriver.DrainAllCommandQueues2(QueueGroup, numGPUs)
	t2 = float64(engine.CurrentTime())
	computeTime := float64(t2 - t1)
	fmt.Printf("Time : %0.12f \n", engine.CurrentTime())
	fmt.Printf("				Reduction Time(CopyToNextGPU) : %0.2f us \n", computeTime*1000000)
	//ForDebugging(gpuDriver, context, SendBuffers, RecvBuffers, 4, numGPUs*ChunkSize)
	//fmt.Scanln()
	/////////////////////////////////////////// ////////////
	//////////Transfer time + Recution per slices//////////
	///////////////////////////////////////////////////////
	TotalOverlapedTime := float64(0)
	lastSliceAddress := 0
	if numSlices >= 2 {
		for j := 2; j <= numSlices; j++ {
			ReduceSrc1Group = make([]driver.GPUPtr, 0)
			ReduceSrc2Group = make([]driver.GPUPtr, 0)
			ReduceDstGroup = make([]driver.GPUPtr, 0)

			SrcAddressGroup = make([]driver.GPUPtr, 0)
			DstAddressGroup = make([]driver.GPUPtr, 0)
			for i := 0; i < numGPUs; i++ {

				//TempSrc1 + TempSrc2 -> TempDst
				//(Recv)   + (Pointer)-> (SendBuffers)
				StartOfChunkAddress := ((numGPUs - (SubStep) + i + 1) % numGPUs) * (1) * ChunkSize * 4
				GPUIndex := i
				TempSrc1Address := RecvBuffers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress+SliceSize*4*(j-1))
				TempSrc2Address := Pointers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress+SliceSize*4*(j-1))
				TempDstAddress := SendBuffers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress+SliceSize*4*(j-1))
				ReduceSrc1Group = append(ReduceSrc1Group, TempSrc1Address)
				ReduceSrc2Group = append(ReduceSrc2Group, TempSrc2Address)
				ReduceDstGroup = append(ReduceDstGroup, TempDstAddress)

				//Copy from (SendBuffers) to (RecvBuffers of Next GPU)
				SrcIndex := i%numGPUs + 1
				DstIndex := (i+1)%numGPUs + 1
				SrcPtr := SendBuffers[SrcIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress+SliceSize*4*(j-2))
				DstPtr := RecvBuffers[DstIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress+SliceSize*4*(j-2))

				SrcAddressGroup = append(SrcAddressGroup, SrcPtr)
				DstAddressGroup = append(DstAddressGroup, DstPtr)
				lastSliceAddress = SliceSize * 4 * (j - 1)
			}

			t1 = float64(engine.CurrentTime())
			KernelOverlapReduceAndCopy(engine, gpuDriver, context, ReduceSrc1Group, ReduceSrc2Group, ReduceDstGroup, SrcAddressGroup, DstAddressGroup, SliceSize*4, numGPUs, HsaCo)
			t2 = float64(engine.CurrentTime())

			overlapedTime := float64(t2 - t1)
			TotalOverlapedTime = overlapedTime + TotalOverlapedTime
			//fmt.Printf("					Mixed Time  : %0.12f ~~~ \n", overlapedTime)
		}
	}
	fmt.Printf("				Reduction + Communication Time(CopyToNextGPU) : %0.2f us \n", TotalOverlapedTime*1000000)

	////////////////////////////////////////////
	//////////Transfer time per slices//////////
	////////////////////////////////////////////

	SrcAddressGroup = make([]driver.GPUPtr, 0)
	DstAddressGroup = make([]driver.GPUPtr, 0)
	for i := 0; i < numGPUs; i++ {
		SrcIndex := i%numGPUs + 1
		DstIndex := (i+1)%numGPUs + 1
		StartOfChunkAddress := ((numGPUs - (SubStep) + i + 1) % numGPUs) * (1) * ChunkSize * 4
		SrcPtr := SendBuffers[SrcIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress+lastSliceAddress)
		DstPtr := RecvBuffers[DstIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress+lastSliceAddress)

		SrcAddressGroup = append(SrcAddressGroup, SrcPtr)
		DstAddressGroup = append(DstAddressGroup, DstPtr)
	}

	t1 = float64(engine.CurrentTime())
	gpuDriver.MultiMemCopyD2D(context, SrcAddressGroup, DstAddressGroup, SliceSize*4, numGPUs) //one slice transfer time
	t2 = float64(engine.CurrentTime())
	pureTransferTime := float64(t2 - t1)
	fmt.Printf("				Communication Time(CopyToNextGPU) : %0.2f us \n", pureTransferTime*1000000)
	//normalBandwidth := float64(float64(SliceSize)*4/pureTransferTime)
	//gBandwidth := float64(normalBandwidth/1000000000)
	//fmt.Printf("				Communication Bandwidth : %f Gbytes/s \n", gBandwidth)
	//	ForDebugging(gpuDriver, context, Pointers, RecvBuffers)

	LaunchTime := float64(0)
	TransferTime := float64(0)
	//return computeTime, TotalOverlapedTime, transferTime
	return float64(LaunchTime), float64(TransferTime)
}

func KernelOverlapReduceAndDirectCopyToNextGPU(engine akita.Engine, gpuDriver *driver.Driver, context *driver.Context, ChunkSize int, SlicesNum int,
	numGPUs int, LoopSize int, currentStep int, SubStep int,
	SendBuffers []driver.GPUPtr, RecvBuffers []driver.GPUPtr, Pointers []driver.GPUPtr, HsaCo *insts.HsaCo) (float64, float64) {
	// PushData
	//	log.Printf("=======ReduceAndCopy=======\n")
	ReduceSrc1Group := make([]driver.GPUPtr, 0)
	ReduceSrc2Group := make([]driver.GPUPtr, 0)
	ReduceDstGroup := make([]driver.GPUPtr, 0)

	SrcAddressGroup := make([]driver.GPUPtr, 0)
	DstAddressGroup := make([]driver.GPUPtr, 0)

	numSlices := SlicesNum
	SliceSize := ChunkSize / numSlices

	t1 := float64(0)
	t2 := float64(0)

	for i := 0; i < numGPUs; i++ {
		GPUIndex := i
		StartOfChunkAddress := ((numGPUs - (SubStep) + i + 1) % numGPUs) * (1) * ChunkSize * 4

		TempSrc1Address := RecvBuffers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		TempSrc2Address := Pointers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		TempDstAddress := SendBuffers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		ReduceSrc1Group = append(ReduceSrc1Group, TempSrc1Address)
		ReduceSrc2Group = append(ReduceSrc2Group, TempSrc2Address)
		ReduceDstGroup = append(ReduceDstGroup, TempDstAddress)
		//fmt.Printf("%d \n", TempDstAddress)

	}

	QueueGroup := make([]*driver.CommandQueue, 0)
	queues := make([]*driver.CommandQueue, numGPUs)
	for i := 0; i < numGPUs; i++ {
		//	dstIndex := i % 4
		//		srcIndex := (i + 1) % 4
		gpuDriver.SelectGPU(context, i+1)
		queues[i] = gpuDriver.CreateCommandQueue(context)
		kernArg := AddKernelArgs{
			ReduceSrc1Group[i],
			ReduceSrc2Group[i],
			ReduceDstGroup[i],
			0,
			int64(i * SliceSize), 0, 0,
		}
		gpuDriver.EnqueueLaunchKernel(
			queues[i],
			HsaCo,
			[3]uint32{uint32(SliceSize), 1, 1},
			[3]uint16{128, 1, 1}, &kernArg,
		)
		QueueGroup = append(QueueGroup, queues[i])
	}

	t1 = float64(engine.CurrentTime())
	gpuDriver.DrainAllCommandQueues2(QueueGroup, numGPUs)
	t2 = float64(engine.CurrentTime())
	firstPartTime := t2 - t1
	fmt.Printf("				Reduction Time(DirectCopyToNextGPU) : %0.2f us \n", float64(firstPartTime)*1000000)
	sumOverlapedTime := float64(0)
	lastSliceAddress := 0
	if numSlices > 2 {
		for j := 2; j <= numSlices; j++ {
			ReduceSrc1Group = make([]driver.GPUPtr, 0)
			ReduceSrc2Group = make([]driver.GPUPtr, 0)
			ReduceDstGroup = make([]driver.GPUPtr, 0)

			SrcAddressGroup = make([]driver.GPUPtr, 0)
			DstAddressGroup = make([]driver.GPUPtr, 0)
			for i := 0; i < numGPUs; i++ {

				GPUIndex := i
				StartOfChunkAddress := ((numGPUs - (SubStep) + i + 1) % numGPUs) * (1) * ChunkSize * 4
				TempSrc1Address := RecvBuffers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress+SliceSize*4*(j-1))
				TempSrc2Address := Pointers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress+SliceSize*4*(j-1))
				TempDstAddress := SendBuffers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress+SliceSize*4*(j-1))
				ReduceSrc1Group = append(ReduceSrc1Group, TempSrc1Address)
				ReduceSrc2Group = append(ReduceSrc2Group, TempSrc2Address)
				ReduceDstGroup = append(ReduceDstGroup, TempDstAddress)

				SrcIndex := i%numGPUs + 1
				DstIndex := (i+1)%numGPUs + 1
				SrcPtr := SendBuffers[SrcIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress+SliceSize*4*(j-2))
				DstPtr := Pointers[DstIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress+SliceSize*4*(j-2))

				SrcAddressGroup = append(SrcAddressGroup, SrcPtr)
				DstAddressGroup = append(DstAddressGroup, DstPtr)
				lastSliceAddress = SliceSize * 4 * (j - 1)
			}
			t1 = float64(engine.CurrentTime())
			KernelOverlapReduceAndCopy(engine, gpuDriver, context, ReduceSrc1Group, ReduceSrc2Group, ReduceDstGroup, SrcAddressGroup, DstAddressGroup, SliceSize*4, numGPUs, HsaCo)
			t2 = float64(engine.CurrentTime())
			overlapedTime := t2 - t1
			//fmt.Printf("			Second Part Time(DirectCopyToNextGPU) : %0.12f \n", float64(overlapedTime))
			sumOverlapedTime = sumOverlapedTime + overlapedTime
		}
	}
	fmt.Printf("				Reduction + Communication Time(DirectCopyToNextGPU) : %0.2f us \n", sumOverlapedTime*1000000)

	SrcAddressGroup = make([]driver.GPUPtr, 0)
	DstAddressGroup = make([]driver.GPUPtr, 0)
	for i := 0; i < numGPUs; i++ {
		SrcIndex := i%numGPUs + 1
		DstIndex := (i+1)%numGPUs + 1
		StartOfChunkAddress := ((numGPUs - (SubStep) + i + 1) % numGPUs) * (1) * ChunkSize * 4
		SrcPtr := SendBuffers[SrcIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress+lastSliceAddress)
		DstPtr := Pointers[DstIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress+lastSliceAddress)

		SrcAddressGroup = append(SrcAddressGroup, SrcPtr)
		DstAddressGroup = append(DstAddressGroup, DstPtr)
	}

	t1 = float64(engine.CurrentTime())
	gpuDriver.MultiMemCopyD2D(context, SrcAddressGroup, DstAddressGroup, SliceSize*4, numGPUs)
	t2 = float64(engine.CurrentTime())
	secondPartTime := float64(t2 - t1)
	fmt.Printf("				Communication Time(DirectCopyToNextGPU) : %0.2f us \n", secondPartTime*1000000)

	//	ForDebugging(gpuDriver, context, Pointers, RecvBuffers)

	LaunchTime := float64(0)
	TransferTime := float64(0)

	return float64(LaunchTime), float64(TransferTime)
}

func KernelOverlapReduceAndCopy(engine akita.Engine, d *driver.Driver,
	ctx *driver.Context, r_src1 []driver.GPUPtr, r_src2 []driver.GPUPtr,
	r_dst []driver.GPUPtr, c_dst []driver.GPUPtr, c_src []driver.GPUPtr,
	size int, numGPUs int, HsaCo *insts.HsaCo) {
	//size = slice size * 4

	QueueGroup := make([]*driver.CommandQueue, 0)

	for i := 0; i < numGPUs; i++ {
		//	dstIndex := i % 4
		//		srcIndex := (i + 1) % 4
		d.SelectGPU(ctx, i+1)
		///////////////////Reduction/////////////////////
		reduction_queue := d.CreateCommandQueue(ctx)
		kernArg := AddKernelArgs{
			r_src1[i],
			r_src2[i],
			r_dst[i],
			0,
			int64(i * (size / numGPUs)), 0, 0,
		}
		d.EnqueueLaunchKernel(
			reduction_queue, HsaCo,
			[3]uint32{uint32(size / numGPUs), 1, 1}, //gridsize
			[3]uint16{128, 1, 1}, &kernArg,          //wgsize, kernelArgs interface
		)
		QueueGroup = append(QueueGroup, reduction_queue)

		///////////////////Transfer//////////////////////
		transfer_queue := d.CreateCommandQueue(ctx)
		d.EnqueueMemCopyD2D(transfer_queue, c_dst[i], c_src[i], size)
		QueueGroup = append(QueueGroup, transfer_queue)
	}

	Time := d.DrainAllCommandQueues2(QueueGroup, numGPUs*2)
	fmt.Printf("		Overlapped Time : %0.2f us \n", Time*1000000)
}
