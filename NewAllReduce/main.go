package main

import (
	"fmt"
	"log"
	"sync"

	"gitlab.com/akita/mgpusim/benchmarks/dnn/kernels/add"
	"gitlab.com/akita/mgpusim/driver"
	"gitlab.com/akita/mgpusim/insts"
	"gitlab.com/akita/mgpusim/kernels"
	"gitlab.com/akita/mgpusim/platform"
)

type AddKernelArgs struct {
	A                   driver.GPUPtr
	B                   driver.GPUPtr
	C                   driver.GPUPtr
	Padding             uint32
	HiddenGlobalOffsetX int64
	HiddenGlobalOffsetY int64
	HiddenGlobalOffsetZ int64
}
type DIVIDKernelArgs struct {
	A                   driver.GPUPtr
	B                   driver.GPUPtr
	C                   driver.GPUPtr
	Padding             uint32
	HiddenGlobalOffsetX int64
	HiddenGlobalOffsetY int64
	HiddenGlobalOffsetZ int64
}

func main() {
	engine, gpuDriver := platform.MakeR9NanoBuilder().Build()
	gpuDriver.Run()
	context := gpuDriver.Init()

	var Hsaco *insts.HsaCo
	hsacoBytes, err := add.Asset("add_kernels.hsaco")
	if err != nil {
		log.Panic(err)
	}
	Hsaco = kernels.LoadProgramFromMemory(hsacoBytes, "VectorADD")

	var wg sync.WaitGroup

	DataLength := 1048576
	ChunkSize := 262144

	SendBuffers := make([]driver.GPUPtr, 4)
	RecvBuffers := make([]driver.GPUPtr, 4)
	tempBuffers := make([]driver.GPUPtr, 4)

	TempArray := make([]float32, DataLength)

	//contexts := make([]*driver.Context, 4)

	fmt.Printf("Datalength : %d \n", DataLength)
	for i := 1; i <= 4; i++ {
		//TempContext := gpuDriver.InitWithExistingPID(context)
		//contexts[i-1] = TempContext
		gpuDriver.SelectGPU(context, i)

		for j := 0; j < DataLength; j++ {
			TempArray[j] = float32(i)
		}
		SendBuffers[i-1] = gpuDriver.AllocateMemory(context, uint64(DataLength*4))
		RecvBuffers[i-1] = gpuDriver.AllocateMemory(context, uint64(DataLength*4))
		tempBuffers[i-1] = gpuDriver.AllocateMemory(context, uint64(DataLength*4))

		gpuDriver.MemCopyH2D(context, SendBuffers[i-1], TempArray)
		gpuDriver.MemCopyH2D(context, tempBuffers[i-1], TempArray)
	}

	//TempBuffer := make([]float32, DataLength)
	/*
		for i := 0; i < 4; i++ {
			fmt.Printf("\nGPU %d", i+1)
			gpuDriver.SelectGPU(context, i+1)
			gpuDriver.MemCopyD2H(context, TempBuffer, SendBuffers[i])
			//for i := 0; i < DataLength; i++ {
			//	fmt.Printf("(%.1f)", TempBuffer[i])
			//}
		}
	*/
	SrcAddressGroup := make([]driver.GPUPtr, 0)
	DstAddressGroup := make([]driver.GPUPtr, 0)
	for i := 0; i < 4; i++ {
		SrcIndex := i%4 + 1
		DstIndex := (i+1)%4 + 1
		SrcPtr := SendBuffers[SrcIndex-1] + driver.GPUPtr(i*ChunkSize*4)
		DstPtr := RecvBuffers[DstIndex-1] + driver.GPUPtr(i*ChunkSize*4)
		SrcAddressGroup = append(SrcAddressGroup, SrcPtr)
		DstAddressGroup = append(DstAddressGroup, DstPtr)
	}
	//fmt.Scanln()

	start := engine.CurrentTime()
	for i := 0; i < 4; i++ {
		wg.Add(1)
		//gpuDriver.SelectGPU(context, i+1)
		if i == 0 {
			go func1(gpuDriver, context, SrcAddressGroup[i], DstAddressGroup[i], ChunkSize, &wg)
		} else if i == 1 {
			go func2(gpuDriver, context, SrcAddressGroup[i], DstAddressGroup[i], ChunkSize, &wg)
		} else if i == 2 {
			go func3(gpuDriver, context, SrcAddressGroup[i], DstAddressGroup[i], ChunkSize, &wg)
		} else if i == 3 {
			go func4(gpuDriver, context, SrcAddressGroup[i], DstAddressGroup[i], ChunkSize, &wg)
		}
	}
	wg.Wait()
	end := engine.CurrentTime()
	fmt.Printf("%0.2f \n", float64(end-start)*1e9)
	fmt.Printf("\ncopy Soruce to Source %0.2f GB/s\n", float64(ChunkSize*4)/(float64(end-start)*float64(1e9)))

	var wg1 sync.WaitGroup
	start1 := engine.CurrentTime()
	for i := 0; i < 4; i++ {
		wg1.Add(1)
		//gpuDriver.SelectGPU(context, i+1)
		if i == 0 {
			go func5(gpuDriver, context, SrcAddressGroup[i], SrcAddressGroup[i], ChunkSize, Hsaco, &wg1)
		} else if i == 1 {
			go func6(gpuDriver, context, SrcAddressGroup[i], SrcAddressGroup[i], ChunkSize, Hsaco, &wg1)
		} else if i == 2 {
			go func7(gpuDriver, context, SrcAddressGroup[i], SrcAddressGroup[i], ChunkSize, Hsaco, &wg1)
		} else if i == 3 {
			go func8(gpuDriver, context, SrcAddressGroup[i], SrcAddressGroup[i], ChunkSize, Hsaco, &wg1)
		}
	}
	wg1.Wait()
	end1 := engine.CurrentTime()

	fmt.Printf("%0.2f \n", float64(end1-start1)*1e9)
	fmt.Printf("\nreduction local to local %0.2f GB/s\n", float64(ChunkSize*4)/(float64(end1-start1)*float64(1000000000)))
	/*
		for i := 0; i < 4; i++ {
			fmt.Printf("\nGPU %d", i+1)
			gpuDriver.MemCopyD2H(context, TempBuffer, RecvBuffers[i])
			for i := 0; i < DataLength; i++ {
				fmt.Printf("(%.1f)", TempBuffer[i])
			}
		}
	*/
}

func func1(gpuDriver *driver.Driver, context *driver.Context, sendBuffers driver.GPUPtr, recvBuffers driver.GPUPtr, chunksize int, wg *sync.WaitGroup) {
	defer wg.Done()

	gpuDriver.SelectGPU(context, 1)
	gpuDriver.MemCopyD2D(context, recvBuffers, sendBuffers, chunksize*4)
}
func func2(gpuDriver *driver.Driver, context *driver.Context, sendBuffers driver.GPUPtr, recvBuffers driver.GPUPtr, chunksize int, wg *sync.WaitGroup) {
	defer wg.Done()

	gpuDriver.SelectGPU(context, 2)
	gpuDriver.MemCopyD2D(context, recvBuffers, sendBuffers, chunksize*4)
}
func func3(gpuDriver *driver.Driver, context *driver.Context, sendBuffers driver.GPUPtr, recvBuffers driver.GPUPtr, chunksize int, wg *sync.WaitGroup) {
	defer wg.Done()

	gpuDriver.SelectGPU(context, 3)
	gpuDriver.MemCopyD2D(context, recvBuffers, sendBuffers, chunksize*4)
}
func func4(gpuDriver *driver.Driver, context *driver.Context, sendBuffers driver.GPUPtr, recvBuffers driver.GPUPtr, chunksize int, wg *sync.WaitGroup) {
	defer wg.Done()

	gpuDriver.SelectGPU(context, 4)
	gpuDriver.MemCopyD2D(context, recvBuffers, sendBuffers, chunksize*4)
}

func func5(
	gpuDriver *driver.Driver, context *driver.Context, sendBuffers driver.GPUPtr,
	recvBuffers driver.GPUPtr, chunksize int, Hsaco *insts.HsaCo, wg *sync.WaitGroup) {
	defer wg.Done()

	gpuDriver.SelectGPU(context, 1)
	queue := gpuDriver.CreateCommandQueue(context)
	kernArg := AddKernelArgs{
		sendBuffers,
		sendBuffers,
		recvBuffers,
		0,
		int64(chunksize), 0, 0,
	}

	gpuDriver.EnqueueLaunchKernel(
		queue,
		Hsaco,
		[3]uint32{uint32(chunksize), 1, 1},
		[3]uint16{128, 1, 1}, &kernArg,
	)
	gpuDriver.DrainCommandQueue(queue)
}
func func6(
	gpuDriver *driver.Driver, context *driver.Context, sendBuffers driver.GPUPtr,
	recvBuffers driver.GPUPtr, chunksize int, Hsaco *insts.HsaCo, wg *sync.WaitGroup) {
	defer wg.Done()

	gpuDriver.SelectGPU(context, 2)
	queue := gpuDriver.CreateCommandQueue(context)
	kernArg := AddKernelArgs{
		sendBuffers,
		sendBuffers,
		recvBuffers,
		0,
		int64(chunksize), 0, 0,
	}

	gpuDriver.EnqueueLaunchKernel(
		queue,
		Hsaco,
		[3]uint32{uint32(chunksize), 1, 1},
		[3]uint16{128, 1, 1}, &kernArg,
	)
	gpuDriver.DrainCommandQueue(queue)
}
func func7(
	gpuDriver *driver.Driver, context *driver.Context, sendBuffers driver.GPUPtr,
	recvBuffers driver.GPUPtr, chunksize int, Hsaco *insts.HsaCo, wg *sync.WaitGroup) {
	defer wg.Done()

	gpuDriver.SelectGPU(context, 3)
	queue := gpuDriver.CreateCommandQueue(context)
	kernArg := AddKernelArgs{
		sendBuffers,
		sendBuffers,
		recvBuffers,
		0,
		int64(chunksize), 0, 0,
	}

	gpuDriver.EnqueueLaunchKernel(
		queue,
		Hsaco,
		[3]uint32{uint32(chunksize), 1, 1},
		[3]uint16{128, 1, 1}, &kernArg,
	)
	gpuDriver.DrainCommandQueue(queue)
}
func func8(
	gpuDriver *driver.Driver, context *driver.Context, sendBuffers driver.GPUPtr,
	recvBuffers driver.GPUPtr, chunksize int, Hsaco *insts.HsaCo, wg *sync.WaitGroup) {
	defer wg.Done()

	gpuDriver.SelectGPU(context, 4)
	queue := gpuDriver.CreateCommandQueue(context)
	kernArg := AddKernelArgs{
		sendBuffers,
		sendBuffers,
		recvBuffers,
		0,
		int64(chunksize), 0, 0,
	}

	gpuDriver.EnqueueLaunchKernel(
		queue,
		Hsaco,
		[3]uint32{uint32(chunksize), 1, 1},
		[3]uint16{128, 1, 1}, &kernArg,
	)
	gpuDriver.DrainCommandQueue(queue)
}
