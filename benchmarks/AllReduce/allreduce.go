package allReduce

import (
	"log"
	"sync"

	"gitlab.com/akita/mgpusim/driver"
)

type AddKernelArgs struct {
	A                   driver.GPUPtr
	B                   driver.GPUPtr
	C                   driver.GPUPtr
	Padding             uint32
	HiddenGlobalOffsetX int64
	HiddenGlobalOffsetY int64
	HiddenGlobalOffsetZ int64
}

type Benchmark struct {
	driver  *driver.Driver
	context *driver.Context

	SendBuffers []driver.GPUPtr
	RecvBuffers []driver.GPUPtr

	DataLength int
	ChunkSize  int

	gpus []int
}

func NewBenchmark(driver *driver.Driver) *Benchmark {
	b := new(Benchmark)
	b.driver = driver
	b.context = driver.Init()

	return b
}

func (b *Benchmark) SelectGPU(gpus []int) {
	b.gpus = gpus
}
func (b *Benchmark) Verify() {

}
func (b *Benchmark) SetUnifiedMemory() {
	log.Printf("Does not support")
}

func (b *Benchmark) Run() {
	b.initMem()
	b.exec()
}

func (b *Benchmark) initMem() {
	b.DataLength = 65536
	b.ChunkSize = 16384
	TempArray := make([]float32, b.DataLength)
	b.SendBuffers = make([]driver.GPUPtr, 4)
	b.RecvBuffers = make([]driver.GPUPtr, 4)
	for i := 1; i <= 4; i++ {
		b.driver.SelectGPU(b.context, i)

		for j := 0; j < b.DataLength; j++ {
			TempArray[j] = float32(i)
		}
		b.SendBuffers[i-1] = b.driver.AllocateMemory(b.context, uint64(b.DataLength*4))
		b.RecvBuffers[i-1] = b.driver.AllocateMemory(b.context, uint64(b.DataLength*4))

		b.driver.MemCopyH2D(b.context, b.SendBuffers[i-1], TempArray)
	}
}

func (b *Benchmark) exec() {
	SrcAddressGroup := make([]driver.GPUPtr, 0)
	DstAddressGroup := make([]driver.GPUPtr, 0)
	for i := 0; i < 4; i++ {
		SrcIndex := i%4 + 1
		DstIndex := (i+1)%4 + 1
		SrcPtr := b.SendBuffers[SrcIndex-1] + driver.GPUPtr(i*b.ChunkSize*4)
		DstPtr := b.RecvBuffers[DstIndex-1] + driver.GPUPtr(i*b.ChunkSize*4)
		SrcAddressGroup = append(SrcAddressGroup, SrcPtr)
		DstAddressGroup = append(DstAddressGroup, DstPtr)
	}

	var wg sync.WaitGroup
	for i := 0; i < 4; i++ {
		wg.Add(1)
		//gpuDriver.SelectGPU(context, i+1)
		if i == 0 {
			go b.func1(b.driver, b.context, SrcAddressGroup[i], DstAddressGroup[i], b.ChunkSize, &wg)
		} else if i == 1 {
			go b.func2(b.driver, b.context, SrcAddressGroup[i], DstAddressGroup[i], b.ChunkSize, &wg)
		} else if i == 2 {
			go b.func3(b.driver, b.context, SrcAddressGroup[i], DstAddressGroup[i], b.ChunkSize, &wg)
		} else if i == 3 {
			go b.func4(b.driver, b.context, SrcAddressGroup[i], DstAddressGroup[i], b.ChunkSize, &wg)
		}
	}
	wg.Wait()
}

func (b *Benchmark) func1(gpuDriver *driver.Driver, context *driver.Context, sendBuffers driver.GPUPtr, recvBuffers driver.GPUPtr, chunksize int, wg *sync.WaitGroup) {
	defer wg.Done()

	gpuDriver.SelectGPU(context, 1)
	gpuDriver.MemCopyD2D(context, recvBuffers, sendBuffers, chunksize*4)
}
func (b *Benchmark) func2(gpuDriver *driver.Driver, context *driver.Context, sendBuffers driver.GPUPtr, recvBuffers driver.GPUPtr, chunksize int, wg *sync.WaitGroup) {
	defer wg.Done()

	gpuDriver.SelectGPU(context, 2)
	gpuDriver.MemCopyD2D(context, recvBuffers, sendBuffers, chunksize*4)
}
func (b *Benchmark) func3(gpuDriver *driver.Driver, context *driver.Context, sendBuffers driver.GPUPtr, recvBuffers driver.GPUPtr, chunksize int, wg *sync.WaitGroup) {
	defer wg.Done()

	gpuDriver.SelectGPU(context, 3)
	gpuDriver.MemCopyD2D(context, recvBuffers, sendBuffers, chunksize*4)
}
func (b *Benchmark) func4(gpuDriver *driver.Driver, context *driver.Context, sendBuffers driver.GPUPtr, recvBuffers driver.GPUPtr, chunksize int, wg *sync.WaitGroup) {
	defer wg.Done()

	gpuDriver.SelectGPU(context, 4)
	gpuDriver.MemCopyD2D(context, recvBuffers, sendBuffers, chunksize*4)
}
