package CCL

import (
	"log"

	"gitlab.com/akita/mgpusim/benchmarks/dnn/kernels/add"
	"gitlab.com/akita/mgpusim/benchmarks/dnn/kernels/division"
	"gitlab.com/akita/mgpusim/kernels"

	"gitlab.com/akita/mgpusim/driver"
	"gitlab.com/akita/mgpusim/insts"
)

type AddKernelArgs struct {
	A                   driver.GPUPtr
	B                   driver.GPUPtr
	C                   driver.GPUPtr
	Padding             uint32
	HiddenGlobalOffsetX int64
	HiddenGlobalOffsetY int64
	HiddenGlobalOffsetZ int64
}

type DIVIDKernelArgs struct {
	A                   driver.GPUPtr
	B                   driver.GPUPtr
	C                   driver.GPUPtr
	Padding             uint32
	HiddenGlobalOffsetX int64
	HiddenGlobalOffsetY int64
	HiddenGlobalOffsetZ int64
}

type Communicator struct {
	Driver  *driver.Driver
	Context *driver.Context

	SendBuffers []driver.GPUPtr
	RecvBuffers []driver.GPUPtr
	DivisionNum []driver.GPUPtr
	Datalength  int
	Chunk       int
	GPUs        int

	LoopSize int
	Hsaco    *insts.HsaCo
	D_Hsaco  *insts.HsaCo
}

func (c Communicator) AllReduce() {
	saveAddress := 0

	hsacoBytes, err := add.Asset("add_kernels.hsaco")
	if err != nil {
		log.Panic(err)
	}
	c.Hsaco = kernels.LoadProgramFromMemory(hsacoBytes, "VectorADD")

	dividehsacoBytes, err := division.Asset("division_kernels.hsaco")
	if err != nil {
		log.Panic(err)
	}
	c.D_Hsaco = kernels.LoadProgramFromMemory(dividehsacoBytes, "VectorDivision")

	LoopSize := c.LoopSize
	c.Chunk = LoopSize / c.GPUs
	for i := 0; i < int(c.Datalength)/LoopSize; i++ {
		c.PushDataToNextGPU(LoopSize, i)
		c.OverlapReduceAndCopyToNextGPU(c.Hsaco, LoopSize, i, 2)
		c.OverlapReduceAndCopyToNextGPU(c.Hsaco, LoopSize, i, 3)
		c.OverlapReduceAndDirectCopyToNextGPU(c.Hsaco, LoopSize, i, 4)
		for j := 2; j <= int(c.GPUs); j++ {
			c.DirectCopyToNextGPU(LoopSize, i, j)
		}
		saveAddress = i
	}

	if c.Datalength%LoopSize != 0 {
		Temp := int(c.Datalength) % LoopSize
		c.Chunk = Temp / c.GPUs
		c.PushDataToNextGPU(LoopSize, saveAddress+1)
		c.OverlapReduceAndCopyToNextGPU(c.Hsaco, LoopSize, saveAddress+1, 2)
		c.OverlapReduceAndCopyToNextGPU(c.Hsaco, LoopSize, saveAddress+1, 3)
		c.OverlapReduceAndDirectCopyToNextGPU(c.Hsaco, LoopSize, saveAddress+1, 4)
		for j := 2; j <= int(c.GPUs); j++ {
			c.DirectCopyToNextGPU(LoopSize, saveAddress+1, j)
		}
	}
	c.DivideLocalGradients()
}

func (c Communicator) PushDataToNextGPU(
	LoopSize int, currentStep int,
) float64 {
	SrcAddressGroup := make([]driver.GPUPtr, 0)
	DstAddressGroup := make([]driver.GPUPtr, 0)
	ChunkSize := int(c.Chunk)
	gpus := c.GPUs
	for i := 0; i < gpus; i++ {
		SrcIndex := i%gpus + 1
		DstIndex := (i+1)%gpus + 1
		SrcPtr := c.SendBuffers[SrcIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+(i*ChunkSize)*4)
		DstPtr := c.RecvBuffers[DstIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+(i*ChunkSize)*4)
		SrcAddressGroup = append(SrcAddressGroup, SrcPtr)
		DstAddressGroup = append(DstAddressGroup, DstPtr)
	}
	c.Driver.MultiMemCopyD2D(c.Context, DstAddressGroup, SrcAddressGroup, ChunkSize*4, gpus)

	return float64(0)
}

func (c Communicator) OverlapReduceAndCopyToNextGPU(
	Hsaco *insts.HsaCo, LoopSize int, currentStep int, subStep int,
) float64 {
	ReduceSrc1Group := make([]driver.GPUPtr, 0)
	ReduceSrc2Group := make([]driver.GPUPtr, 0)
	ReduceDstGroup := make([]driver.GPUPtr, 0)
	ChunkSize := c.Chunk
	gpus := c.GPUs
	for i := 0; i < gpus; i++ {
		//TempSrc1 + TempSrc2 -> TempDst
		//(Recv)   + (Pointer)-> (SendBuffers)
		StartOfChunkAddress := ((gpus - (subStep) + i + 1) % gpus) * (1) * ChunkSize * 4
		GPUIndex := i
		TempSrc1Address := c.RecvBuffers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		TempSrc2Address := c.SendBuffers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		TempDstAddress := c.RecvBuffers[(GPUIndex+1)%4] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		ReduceSrc1Group = append(ReduceSrc1Group, TempSrc1Address)
		ReduceSrc2Group = append(ReduceSrc2Group, TempSrc2Address)
		ReduceDstGroup = append(ReduceDstGroup, TempDstAddress)
	}
	QueueGroup := make([]*driver.CommandQueue, 0)
	queues := make([]*driver.CommandQueue, gpus)

	for i := 0; i < c.GPUs; i++ {
		c.Driver.SelectGPU(c.Context, i+1)
		queues[i] = c.Driver.CreateCommandQueue(c.Context)
		kernArg := AddKernelArgs{
			ReduceSrc1Group[i],
			ReduceSrc2Group[i],
			ReduceDstGroup[i],
			0,
			int64(i * ChunkSize), 0, 0,
		}

		c.Driver.EnqueueLaunchKernel(
			queues[i],
			Hsaco,
			[3]uint32{uint32(c.Chunk), 1, 1},
			[3]uint16{128, 1, 1}, &kernArg,
		)
		QueueGroup = append(QueueGroup, queues[i])
	}

	c.Driver.DrainAllCommandQueues2(QueueGroup, gpus)
	return float64(0)
}

func (c Communicator) OverlapReduceAndDirectCopyToNextGPU(
	Hsaco *insts.HsaCo, LoopSize int, currentStep int, subStep int,
) float64 {
	ReduceSrc1Group := make([]driver.GPUPtr, 0)
	ReduceSrc2Group := make([]driver.GPUPtr, 0)
	ReduceDstGroup := make([]driver.GPUPtr, 0)
	ChunkSize := c.Chunk
	gpus := c.GPUs
	for i := 0; i < gpus; i++ {
		//TempSrc1 + TempSrc2 -> TempDst
		//(Recv)   + (Pointer)-> (SendBuffers)
		StartOfChunkAddress := ((gpus - (subStep) + i + 1) % gpus) * (1) * ChunkSize * 4
		GPUIndex := i
		TempSrc1Address := c.RecvBuffers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		TempSrc2Address := c.SendBuffers[GPUIndex] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		TempDstAddress := c.SendBuffers[(GPUIndex+1)%4] + driver.GPUPtr(currentStep*LoopSize*4+StartOfChunkAddress)
		ReduceSrc1Group = append(ReduceSrc1Group, TempSrc1Address)
		ReduceSrc2Group = append(ReduceSrc2Group, TempSrc2Address)
		ReduceDstGroup = append(ReduceDstGroup, TempDstAddress)
	}
	QueueGroup := make([]*driver.CommandQueue, 0)
	queues := make([]*driver.CommandQueue, gpus)
	for i := 0; i < gpus; i++ {
		c.Driver.SelectGPU(c.Context, i+1)
		queues[i] = c.Driver.CreateCommandQueue(c.Context)
		kernArg := AddKernelArgs{
			ReduceSrc1Group[i],
			ReduceSrc2Group[i],
			ReduceDstGroup[i],
			0,
			int64(i * ChunkSize), 0, 0,
		}

		c.Driver.EnqueueLaunchKernel(
			queues[i],
			Hsaco,
			[3]uint32{uint32(ChunkSize), 1, 1},
			[3]uint16{128, 1, 1}, &kernArg,
		)
		QueueGroup = append(QueueGroup, queues[i])
	}
	c.Driver.DrainAllCommandQueues2(QueueGroup, gpus)
	return float64(0)
}
func (c Communicator) DirectCopyToNextGPU(
	LoopSize int, currentStep int, subStep int,
) float64 {

	SrcAddressGroup := make([]driver.GPUPtr, 0)
	DstAddressGroup := make([]driver.GPUPtr, 0)
	ChunkSize := int(c.Chunk)
	gpus := int(c.GPUs)

	for i := 0; i < gpus; i++ {
		SrcIndex := i%gpus + 1     //1, 2, 3, 4
		DstIndex := (i+1)%gpus + 1 //2, 3, 4, 1
		//Reserve the Destination address where they was located.
		SrcPtr := c.SendBuffers[SrcIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+((gpus-subStep+i+2)%gpus*ChunkSize)*4)
		DstPtr := c.SendBuffers[DstIndex-1] + driver.GPUPtr(currentStep*LoopSize*4+((gpus-subStep+i+2)%gpus*ChunkSize)*4)
		SrcAddressGroup = append(SrcAddressGroup, SrcPtr)
		DstAddressGroup = append(DstAddressGroup, DstPtr)
	}

	c.Driver.MultiMemCopyD2D(c.Context, DstAddressGroup, SrcAddressGroup, ChunkSize*4, gpus)
	//ChunkSize*4 --> When they communicated by this unit.

	return float64(0)
}

func (c Communicator) DivideLocalGradients() {
	divideSrc1Group := make([]driver.GPUPtr, 0)
	for i := 0; i < c.GPUs; i++ {
		divideSrc1Group = append(divideSrc1Group, c.SendBuffers[i])
	}

	QueueGroup := make([]*driver.CommandQueue, 0)
	queues := make([]*driver.CommandQueue, c.GPUs)

	for i := 0; i < c.GPUs; i++ {
		c.Driver.SelectGPU(c.Context, i+1)
		queues[i] = c.Driver.CreateCommandQueue(c.Context)
		KernArg := DIVIDKernelArgs{
			divideSrc1Group[i],
			c.DivisionNum[i],
			divideSrc1Group[i],
			0,
			int64(c.Datalength), 0, 0,
		}
		c.Driver.EnqueueLaunchKernel(
			queues[i],
			c.D_Hsaco,
			[3]uint32{uint32(c.Datalength), 1, 1},
			[3]uint16{128, 1, 1}, &KernArg,
		)
		QueueGroup = append(QueueGroup, queues[i])
	}
	c.Driver.DrainAllCommandQueues2(QueueGroup, c.GPUs)
}
