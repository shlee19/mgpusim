package gputraining

import (
	"log"
	"math"
	"sync"

	"gitlab.com/akita/dnn/tensor"
	"gitlab.com/akita/dnn/training"
	"gitlab.com/akita/dnn/training/optimization"

	"gitlab.com/akita/mgpusim/benchmarks/dnn/CCL"
	"gitlab.com/akita/mgpusim/driver"
)

// DataParallelismMultiGPUTrainer can use multiple GPUs to train the DNN model
// in the data parallelism style.
type DataParallelismMultiGPUTrainer struct {
	Driver  *driver.Driver
	Context *driver.Context

	Networks        []training.Network
	DataSource      training.DataSource
	LossFunc        training.LossFunction
	OptimizationAlg optimization.Alg
	Tester          *training.Tester
	Epoch           int
	BatchSize       int
	ShowBatchInfo   bool

	Communicator *CCL.Communicator
	//exchangeBuffers []driver.GPUPtr
	RecvBuffers []driver.GPUPtr
}

// Train will run the training algorithm on the network.
func (t DataParallelismMultiGPUTrainer) Train() {
	//t.allReduceIntialize()

	for currentEpoch := 0; currentEpoch < t.Epoch; currentEpoch++ {
		log.Printf("Epoch %d\n", currentEpoch)

		t.DataSource.Rewind()

		batchNum := 0
		for {
			if t.ShowBatchInfo {
				log.Printf("Batch %d\n", batchNum)
			}
			epochCompleted := t.calculateBatchGradientAllGPUs()
			if epochCompleted {
				break
			}
			//t.averageGradient()
			t.allReduce()
			t.updateParameters()
			batchNum++
		}
		t.test()
	}
}

func (t DataParallelismMultiGPUTrainer) calculateBatchGradientAllGPUs() (
	epochCompleted bool,
) {
	var wg sync.WaitGroup

	for _, network := range t.Networks {
		data, label := t.DataSource.NextBatch(t.BatchSize / len(t.Networks))
		//for i := 0; i < 79; i++ {
		//	fmt.Printf(" %d  ", i)
		//	data, label = t.DataSource.NextBatch(t.BatchSize / len(t.Networks))
		//}

		if len(label) == 0 {
			epochCompleted = true
			break
		}

		wg.Add(1)
		go t.calculateBatchGradientOneGPU(network, data, label, &wg)
	}

	wg.Wait()

	return epochCompleted
}

func (t DataParallelismMultiGPUTrainer) calculateBatchGradientOneGPU(
	network training.Network,
	data tensor.Tensor, label []int,
	wg *sync.WaitGroup,
) {
	defer wg.Done()

	output := t.forward(data, &network)
	derivative := t.calculateLoss(output, label)
	t.backward(derivative, &network)
}

func (t DataParallelismMultiGPUTrainer) forward(
	data tensor.Tensor,
	network *training.Network,
) tensor.Tensor {
	//log.Printf("Forward.\n")
	var input, output tensor.Tensor
	output = data
	for _, l := range network.Layers {
		input = output
		output = l.Forward(input)
	}
	return output
}

func (t DataParallelismMultiGPUTrainer) calculateLoss(
	output tensor.Tensor,
	inputLabel []int,
) *tensor.SimpleTensor {
	loss, derivative := t.LossFunc.Loss(output, inputLabel)

	if t.ShowBatchInfo {
		accuracy := calculateAccuracy(output, inputLabel)
		log.Printf("loss: %f, accuracy %f\n", loss, accuracy)
	}

	return derivative
}

func (t DataParallelismMultiGPUTrainer) backward(
	derivative *tensor.SimpleTensor,
	network *training.Network,
) {
	//log.Printf("Backward.\n")
	var output tensor.Tensor
	output = derivative
	for i := len(network.Layers) - 1; i >= 0; i-- {
		input := output
		output = network.Layers[i].Backward(input)
	}
}

func (t DataParallelismMultiGPUTrainer) updateParameters() {
	log.Printf("Update Parameters.\n")
	for _, n := range t.Networks {
		for _, l := range n.Layers {
			t.OptimizationAlg.UpdateParameters(l)
		}
	}
}

func (t DataParallelismMultiGPUTrainer) averageGradient() {
	//t.allReduce()
	for l := range t.Networks[0].Layers {
		if t.Networks[0].Layers[0].Gradients() == nil {
			continue
		}
		var gradients []tensor.Vector
		for _, n := range t.Networks {
			gradients = append(gradients, n.Layers[l].Gradients())
		}
		for i := range gradients {
			for j := range gradients {
				if i == j {
					continue
				}
				//gradients[i].Add(gradients[j])
				gradients[i].Add(gradients[j])
			}
		}
		for i := range gradients {
			//gradients[i].Scale(1.0 / float64(len(gradients)))
			gradients[i].Scale32(float32(1.0 / float32(len(gradients))))
		}
	}
}

func (t DataParallelismMultiGPUTrainer) allReduce() {
	allReduceBuffers := make([]driver.GPUPtr, t.Driver.GetNumGPUs())
	var allReduceAvailable bool
	for l := range t.Networks[0].Layers {
		for i, n := range t.Networks {
			if n.Layers[l].GradientsNum() == 0 {
				allReduceAvailable = false
				continue
			}
			allReduceAvailable = true
			allReduceBuffers[i] = n.Layers[l].GradientsPtr()
			t.Communicator.Datalength = n.Layers[l].GradientsNum()
			if t.Communicator.Datalength <= 1024*1024 {
				t.Communicator.LoopSize = (n.Layers[l].GradientsNum())
			} else {
				t.Communicator.LoopSize = 1024 * 1024
			}
		}
		//for i := 0; i < 4; i++ {
		//	fmt.Printf(" %d  ", int(allReduceBuffers[i]))
		//}
		//fmt.Printf("\n")
		if allReduceAvailable {
			t.Communicator.SendBuffers = allReduceBuffers
			t.Communicator.AllReduce()
			log.Printf("All-Reduce on %d Layers \n", l)
		}
	}
}

func (t DataParallelismMultiGPUTrainer) test() {
	if t.Tester == nil {
		return
	}

	accuracy := t.Tester.Test()
	log.Printf("Accuracy %f\n", accuracy)
}

func calculateAccuracy(output tensor.Tensor, inputLabel []int) float64 {
	size := output.Size()
	data := output.Vector()
	correct := 0

	for i := 0; i < size[0]; i++ {
		max := -math.MaxFloat64
		maxIndex := -1

		for j := 0; j < size[1]; j++ {
			index := i*size[1] + j
			prob := data[index]
			if prob > max {
				max = prob
				maxIndex = j
			}
		}
		if maxIndex == inputLabel[i] {
			correct++
		}
	}
	return float64(correct) / float64(size[0])
}
