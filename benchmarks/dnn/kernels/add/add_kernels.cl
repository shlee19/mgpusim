__kernel void VectorADD(__global float *A,
        __global float* B,
        __global float* C)
{
    int idx = get_global_id(0);
    C[idx] = A[idx] + B[idx];
}
