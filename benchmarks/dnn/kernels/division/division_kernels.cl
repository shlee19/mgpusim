__kernel void VectorDivision(__global float * A,
        __global float * B,
        __global float * C)
    {
        float divider = (*B);
        int idx = get_global_id(0);
        C[idx] = A[idx] / divider;
    }
