__kernel void VectorADD(__global uint *A,
		__global uint* B,
		__global uint* C)
{
	int idx = get_global_id(0);
	C[idx] = A[idx] + B[idx];
}