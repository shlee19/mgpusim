package sumGPU

import (
	"log"

	"gitlab.com/akita/mgpusim/driver"
	"gitlab.com/akita/mgpusim/insts"
	"gitlab.com/akita/mgpusim/kernels"
)

type KernelArgs struct {
	A                   driver.GPUPtr
	B                   driver.GPUPtr
	C                   driver.GPUPtr
	Padding             uint32
	HiddenGlobalOffsetX int64
	HiddenGlobalOffsetY int64
	HiddenGlobalOffsetZ int64
}

type Benchmark struct {
	driver  *driver.Driver
	context *driver.Context
	queue   *driver.CommandQueue
	Hsaco   *insts.HsaCo
	gpus    []int

	Length int
	A      []uint32
	B      []uint32
	gA     driver.GPUPtr
	gB     driver.GPUPtr
	gC     driver.GPUPtr

	useUnifiedMemory bool
}

func NewBenchmark(driver *driver.Driver) *Benchmark {
	b := new(Benchmark)

	b.driver = driver
	b.context = b.driver.Init()
	b.queue = driver.CreateCommandQueue(b.context)

	hsacoBytes, err := Asset("kernels.hsaco")
	if err != nil {
		log.Panic(err)
	}
	b.Hsaco = kernels.LoadProgramFromMemory(hsacoBytes, "VectorADD")

	return b
}

func (b *Benchmark) SelectGPU(gpus []int) {
	b.gpus = gpus
}
func (b *Benchmark) SetUnifiedMemory() {
	b.useUnifiedMemory = true
}

func (b *Benchmark) Run() {
	b.driver.SelectGPU(b.context, b.gpus[0])
	b.initMem()
	b.exec()
	b.Verify()
}

func (b *Benchmark) initMem() {
	size := uint32(1024)
	// initialize ..
	b.A = make([]uint32, size)
	b.B = make([]uint32, size)

	for i := 0; i < 1024; i++ {
		b.A[i] = uint32(1)
		b.B[i] = uint32(1)
	}

	b.gA = b.driver.AllocateMemory(b.context, uint64(size*4))
	b.gB = b.driver.AllocateMemory(b.context, uint64(size*4))
	b.gC = b.driver.AllocateMemory(b.context, uint64(size*4))

	b.driver.MemCopyH2D(b.context, b.gA, b.A)
	b.driver.MemCopyH2D(b.context, b.gB, b.B)
}

func (b *Benchmark) exec() {
	queues := make([]*driver.CommandQueue, len(b.gpus))
	numWi := b.Length

	for i, gpu := range b.gpus {
		b.driver.SelectGPU(b.context, gpu)
		queues[i] = b.driver.CreateCommandQueue(b.context)

		kernArg := KernelArgs{
			b.gA,
			b.gB,
			b.gA,
			0,
			int64(i * numWi / len(b.gpus)), 0, 0,
		}

		b.driver.EnqueueLaunchKernel(
			queues[i],
			b.Hsaco,
			[3]uint32{uint32(numWi / len(b.gpus)), 1, 1},
			[3]uint16{256, 1, 1}, &kernArg,
		)
	}

	for i := range b.gpus {
		b.driver.DrainCommandQueue(queues[i])
	}
}

func (b *Benchmark) Verify() {
	gpuOutput := make([]uint32, b.Length)
	b.driver.MemCopyD2H(b.context, gpuOutput, b.gA)

	for i := 0; i < 1024; i++ {
		log.Printf("%d ", gpuOutput[i])
	}

	log.Printf("Passed!\n")
}
