module gitlab.com/akita/mgpusim

require (
	github.com/golang/mock v1.4.4
	github.com/onsi/ginkgo v1.14.2
	github.com/onsi/gomega v1.10.3
	github.com/rs/xid v1.2.1
	github.com/tebeka/atexit v0.3.0
	github.com/vbauerster/mpb/v4 v4.12.2
	gitlab.com/akita/akita v1.10.1
	gitlab.com/akita/dnn v0.4.0
	gitlab.com/akita/mem v1.11.0
	gitlab.com/akita/noc v1.4.0
	gitlab.com/akita/util v0.7.0
)

//replace gitlab.com/akita/akita => ../akita2

replace gitlab.com/akita/noc => ../noc-original

replace gitlab.com/akita/mem => ../mem

// replace gitlab.com/akita/util => ../util

replace gitlab.com/akita/dnn => ../dnn

go 1.15
